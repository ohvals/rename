# Rename
Rename plugin for Spigot.

## What does this plugin do?
When changing from creative to surival, this plugin will check the config, and rename the specified item types to the specified value.

When setting items here, you must use the Enum value (i.e. diamond sword = DIAMOND_SWORD).
If you are having troubles finding them, check [here](https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/Material.html).

## Example:
```
# When setting items here, you must use
# the Enum value. (i.e. diamond sword = DIAMOND_SWORD)
# If you are having troubles finding them, check
# https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/Material.html
MUSHROOM_SOUP: "&6First Aid Kit"
```
