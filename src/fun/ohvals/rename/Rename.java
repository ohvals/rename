package fun.ohvals.rename;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Rename was created by dylan on 06/2017.
 * Please do not redistribute without permission of the developer.
 */

public class Rename extends JavaPlugin implements Listener {

    /*
     *  haHAA 2 one classers in 1 day xDdddd kms
     */

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
        saveDefaultConfig();
    }

    @Override
    public void onDisable() {

    }

    @EventHandler
    public void onGamemodeChange(PlayerGameModeChangeEvent e) {
        Player player = e.getPlayer();

        for (ItemStack item : player.getInventory().getContents()) {
            if (!(item == null || item.getType() == Material.AIR)) {
                if (getConfig().contains(item.getType().name())) {
                    ItemMeta meta = item.getItemMeta();
                    meta.setDisplayName(getConfig().getString(item.getType().name()).replace("&", "§"));
                    item.setItemMeta(meta);
                }
            }
        }
    }

}
